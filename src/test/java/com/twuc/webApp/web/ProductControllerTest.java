package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductOrder;
import com.twuc.webApp.domain.ProductOrderRepository;
import com.twuc.webApp.domain.ProductReposiroty;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductControllerTest extends ApiTestBase {
    @Autowired
    private ProductReposiroty productReposiroty;
    @Autowired
    private ProductOrderRepository productOrderRepository;
    @Test
    void should_create_product() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CreateProductRequest createProductRequest = new CreateProductRequest("可乐",2,1,"瓶");
        getMockMvc().perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createProductRequest)))
                .andExpect(status().is(201));
    }

    @Test
    void should_get_product() throws Exception {
        productReposiroty.saveAndFlush(new Product("cola1",2,1,"pin"));
        productReposiroty.saveAndFlush(new Product("cola2",2,1,"pin"));
        getMockMvc().perform(get("/api/product"))
                .andExpect(status().is(200));
    }

    @Test
    void should_get_order() throws Exception {
        productOrderRepository.saveAndFlush(new ProductOrder("tea1",2,1,"pin"));
        productOrderRepository.saveAndFlush(new ProductOrder("tea2",2,1,"pin"));
        getMockMvc().perform(get("/api/product/order"))
                .andExpect(status().is(200));
    }
}