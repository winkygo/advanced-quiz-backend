CREATE TABLE IF NOT EXISTS products (
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    price integer NOT NULL,
    quantity integer NOT NULL,
    unit varchar(255),
    PRIMARY KEY (id)
);