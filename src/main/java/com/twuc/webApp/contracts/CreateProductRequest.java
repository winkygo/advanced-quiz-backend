package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;

public class CreateProductRequest {
    @NotNull
    private String name;
    @NotNull
    private int price;
    @NotNull
    private int quantity;
    @NotNull
    private String unit;

    public CreateProductRequest() {
    }

    public CreateProductRequest(@NotNull String name, @NotNull int price, @NotNull int quantity, @NotNull String unit) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }
}
