package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductOrder;
import com.twuc.webApp.domain.ProductOrderRepository;
import com.twuc.webApp.domain.ProductReposiroty;
import com.twuc.webApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/product")
@CrossOrigin(origins = "*")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ProductReposiroty productReposiroty;

    @Autowired
    private ProductOrderRepository productOrderRepository;

    @PostMapping
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest){
        Product product = productService.createProduct(createProductRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity<List<Product>> getProduct(){
        List<Product> products = productReposiroty.findAll();
        return ResponseEntity.status(200).body(products);
    }

    @GetMapping(value = "/order")
    public ResponseEntity<List<ProductOrder>> getProductOrder(){
        List<ProductOrder> productOrders = productOrderRepository.findAll();
        return ResponseEntity.status(200).body(productOrders);
    }
}
