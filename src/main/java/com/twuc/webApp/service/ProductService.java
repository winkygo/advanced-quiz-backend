package com.twuc.webApp.service;

import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductReposiroty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    @Autowired
    private ProductReposiroty productReposiroty;

    public Product createProduct(CreateProductRequest createProductRequest){
        Product product = new Product(createProductRequest.getName(),
                createProductRequest.getPrice(),
                createProductRequest.getQuantity(),
                createProductRequest.getUnit());
        return productReposiroty.save(product);
    }
}
